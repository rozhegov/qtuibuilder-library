#ifndef LINKERGUI_H
#define LINKERGUI_H

#include <QObject>
#include <QPair>
#include <QtWidgets>
#include <stdlib.h>
#include <functional>
#include <QMetaType>

#include "confparser.h"
#include "cppgcontrol.h"
#include "mainwindow.h"

class MainWindow;

typedef QList<QPair<BitAdress,int> > (MainWindow::*arrayFunc_t)(int);
typedef int(MainWindow::*intFunc_t)(int);

Q_DECLARE_METATYPE(intFunc_t)
Q_DECLARE_METATYPE(arrayFunc_t)

class LinkerGUI : public QObject
   {
   Q_OBJECT

private:
   ConfParser* cp;
   QWidget* currentWidget;
   afe_regs* regs;
   MainWindow* window;
   LinkHash linkHash;

   BitAdress getBitAdress(BitProperties bf);
   BitProperties* findBitField(QString objectName);
   QString objNameToStr(QString objName);
   void printFields(bool isUsed);
   void takeCheckBox(QCheckBox* checkBox);
   void takeSpinBox(QSpinBox* spinBox);
   void takeComboBox(QComboBox* comboBox);

public:
   QHash<QString, intFunc_t > specFunctionsHash;
   QHash<QString, arrayFunc_t > specArrayFunctionsHash;
   QList<QWidget*> widgets;

   LinkerGUI(){}
   void init(QString adress);
   void setWindow(MainWindow* mw){ window = mw; }
   void setRegs(afe_regs *value){ regs = value; }
   void startFunc();

   LinkHash getLinkHash() const { return linkHash; }
   uint24_t getRegister(int index) { return this->regs->raw[index]; }

   void appendSpecialMethodsHash(QString regName, arrayFunc_t func);
   void appendSpecialMethodsHash(QString regName, intFunc_t func);
   bool functionExists(QString str);
   void writeValueDirected(int value, int index);
   void writeValueByAdress(BitAdress adr, int value);
   void writeRegister(BitAdress adr, int value, bool shouldEmit = true);

public slots:
   void writeValueByRegName(const QString objectName, int value, bool shouldWriteRaw = false);
signals:
   void regsChanged();
   void regsChanged(BitAdress a);

   };

#endif // LINKERGUI_H
