#ifndef CONFPARSER_H
#define CONFPARSER_H

#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QHash>
#include <QDebug>
#include <QTextStream>
#include <QMetaType>

#include "bihash.h"

class BitAdress;
Q_DECLARE_METATYPE(BitAdress)

enum Atribute
   {
   A_None = 0,
   A_Write,
   A_Read,
   A_ReadWrite = A_Write | A_Read
   };

struct BitProperties
   {
   ushort atributes=0;
   bool connected = false;
   QString specialMethodName;
   inline void clear(){atributes=0, clearTemp();}
   inline void clearTemp(){connected = false, specialMethodName.clear();}
   };

class BitAdress
   {
public:
   BitAdress(){}
   BitAdress(int _index){initialize(_index);}
   BitAdress(int _index, int _lastBit, int _initBit){initialize(_index,_lastBit,_initBit);}
   ~BitAdress(){}
   int index = -1;
   int initBit = -1, lastBit =-1;

   operator bool() const { return index != -1 && lastBit != -1 && initBit != -1; }

   BitAdress indexInc(){index+=1; return *this;}
   void initialize(int _index, int _lastBit=24, int _initBit=0){
      index=_index;
      lastBit=_lastBit;
      initBit=_initBit;
      }
   inline void clear(){ index = -1; clearTemp(); }
   inline void clearTemp(){ initBit = -1, lastBit =-1; }
   };

inline uint qHash(const BitAdress &key, uint seed) { return qHash(key.index, seed) ^  key.initBit; }
QDebug operator<<(QDebug dbg, const BitAdress &ba);


class LinkHash : public BiHash<BitAdress,QString,BitProperties>
   {
public:
   LinkHash(){}
   int regLen;
   };

class ConfParser
   {
   QString filePath;

   BitAdress bitAdress;
   QString bitFieldName;
   BitProperties bitProperties;
   QStringList initRegNames;

   LinkHash* linkHash;
public:
   ConfParser(QString filePath, LinkHash& _linkHash);
   void setPath(QString fileName);
   void initializeParsing();

private:
   void insertToHash();
   void clearHashPackage(bool fullClear);
   void startParsing(QString line);
   void readAtributes(QString str);
   void readAdress(QString str);
   void readRegLine(QString str);
   void readSingleRegister(QString str);
   void readInitRegName(QString str);
   void readBitScope(QString str);
   void readSpecialMethod(QString str);
   };

#endif // CONFPARSER_H
